************************
metal-hydrogen-diffusion
************************


What does it do?
================

This code is provided to support the paper "Effect of anisotropy and regime of
diffusion on the measurement of lattice diffusion coefficient of hydrogen in
metals"


Dependencies
============

This project depends on the core components of the FEniCS project
(https://fenicsproject.org/).

This project further requires python 3, multiprocessing, matplotlib and numpy.


Contributors
============

* Nate J. C. Sime njcs4@cam.ac.uk
* Arun Raina

License
=======

GNU LGPL, version 3.